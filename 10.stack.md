
---

TeamEverywhere Javascript Guide
*Updated at 2020-02-23*

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [TeamEverywhere](https://www.team-everywhere.com/)*

- [Stack](#Stack)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


## Stack
  - Queue와 유사하지만 Last-In-First-Out의 특성을 가진 데이터 구조 입니다.
  - 프론트 개발에서 화면의 뷰를 구성할 때 많이 사용되는 자료구조입니다.
  - 일반적으로 Stack에 등록하는 것을 push, 해제하는 것을 pop라고 합니다.
  - Queue와 유사하니 LinkedList를 활용하여 한번 만들어보겠습니다.
  
```javascript
  class Stack {
    constructor() {
      this.linkedList = new LinkedList();
    }
    isEmpty() {
      return !this.linkedList.head;
    }
  
    peek() {
      if (this.isEmpty()) {
        return null;
      }
  
      return this.linkedList.head.value;
    }
  
    push(value) {
      this.linkedList.prepend(value);
    }
  
    pop() {
      const removedHead = this.linkedList.deleteHead();
      return removedHead ? removedHead.value : null;
    }
  
    toArray() {
      return this.linkedList
        .toArray()
        .map(linkedListNode => linkedListNode.value);
    }
  
    toString(callback) {
      return this.linkedList.toString(callback);
    }
  }
```
  