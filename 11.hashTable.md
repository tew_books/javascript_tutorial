
---

TeamEverywhere Javascript Guide
*Updated at 2020-02-23*

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [TeamEverywhere](https://www.team-everywhere.com/)*

- [HashTable](#HashTable)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


## HashTable
  - key와 value로 이루어진 데이터 구조입니다.
  - hash table의 경우, 키에 맞게 저장된 데이터를 찾기 위해서 해쉬 함수를 사용합니다.
<img src="hashtable.svg" width="315" height="230" />

  
```javascript
  const defaultHashTableSize = 32;


  class HashTable {
    
    constructor(hashTableSize = defaultHashTableSize) {
      // 일정 size의 LinkedList 배열을 만듭니다.
      this.buckets = Array(hashTableSize).fill(null).map(() => new LinkedList());

      // 실제 키를 저장할 오브젝트를 만듭니다.
      this.keys = {};
    }

    
    // 키를 해쉬 숫자로 만드는 코드 입니다.
    hash(key) {
      // 해쉬함수를 사용하여 키를 만들어 봅시다.
      const hash = Array.from(key).reduce(
        (hashAccumulator, keySymbol) => (hashAccumulator + keySymbol.charCodeAt(0)),
        0,
      );
      console.log('hash : ', hash)
      // hash size에 맞게 반환을 합니다.
      return hash % this.buckets.length;
    }

    
    set(key, value) {
      const keyHash = this.hash(key);
      //키 해쉬에 hash를 할당합니다.
      console.log('keyHash : ',keyHash)
      this.keys[key] = keyHash;
      const bucketLinkedList = this.buckets[keyHash];
      //리스트에 없을 경우, 빈 값의 node를 받습니다.
      console.log('bucketLinkedList : ',bucketLinkedList)
      const node = bucketLinkedList.find({ callback: nodeValue => nodeValue.key === key });
      // bucketLinkedList에서 value의 key가 같은 값이 있으면 반환합니다.
      if (!node) {
        //기존에 node가 없을 경우 새 값을 넣습니다.
        bucketLinkedList.append({ key, value });
      } else {
        //기존에 node가 있을 경우, 업데이트를 합니다.
        node.value.value = value;
      }
    }

    delete(key) {
      const keyHash = this.hash(key);
      delete this.keys[key];
      const bucketLinkedList = this.buckets[keyHash];
      const node = bucketLinkedList.find({ callback: nodeValue => nodeValue.key === key });

      if (node) {
        return bucketLinkedList.delete(node.value);
      }

      return null;
    }

    get(key) {
      const bucketLinkedList = this.buckets[this.hash(key)];
      const node = bucketLinkedList.find({ callback: nodeValue => nodeValue.key === key });

      return node ? node.value.value : undefined;
    }

    has(key) {
      return Object.hasOwnProperty.call(this.keys, key);
      //객체가 특정 property를 가지고 있는 지 확인할 때 사용하는 함수 입니다.
    }

    getKeys() {
      return Object.keys(this.keys);
      // 객체의 키들을 반환합니다.
    }
  }
```
  