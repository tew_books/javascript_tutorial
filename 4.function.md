


---

TeamEverywhere Javascript Guide
*Updated at 2022-07-08*

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [TeamEverywhere](https://www.team-everywhere.com/)*

- [정의](#정의)
- [Anonymous_function(익명함수)](#Anonymous_function(익명함수))
- [Parameters(매개변수)](#Parameters(매개변수))
- [Inside_function](#Inside_function)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## 정의
  - 함수(function)란 특정 기능 및 데이터를 가진 코드의 집합입니다.
  - 프로그래밍은 결국, 함수를 계속 만들고 조합하여 하나의 완성된 기능을 만들어내는 것이라고도 할 수 있습니다.
  ```javascript
  function log(){
    console.log("log!")
  }
  log()
  ```
 

## Parameters(매개변수)
  - 함수의 결과를 도출하기 위해 필요한 변수들을 함수 선언 시 같이 선언해줄 수 있습니다.
  - 아래 코드의 num1, num2가 파라미터이며, 왜 필요한지는 내부에 선언된 코드를 통해 알 수 있습니다.
  ```Javascript
    function calculate(num1,num2) {
      let result = num1 + num2;
      return result;
    }
    const result = calculate(1, 3);
    console.log("result : ",result);
  ```
  - 매개 변수가 없다면 위의 계산기능을 이용할 때 아래와 같이 해야 하는 끔찍한 상황이 발생할 겁니다!
  ```Javascript
    let numberOne = 1;
    let numberTwo = 2;
    let numberThree = 3;
    let numberFour = 4;
    function calculate1() {
      let result = numberOne + numberTwo;
      return result;
    }
    function calculate2() {
      let result = numberThree + numberFour;
      return result;
    }
    const result1 = calculate1();
    const result2 = calculate2();
    console.log("result1 : ",result1);
    console.log("result2 : ",result2);
  ```


## Anonymous_function(익명함수)
  - 말 그대로 이름이 없는 함수
  ```Javascript
  let anonymousFunction = function() {
    console.log('wow');
  }
  anonymousFunction();  
  ```
  - 위 예는 예시를 위해 설명했으나, 기본적으로는 위와 같이 사용하지 않는 것을 권합니다.
  - '왜 그럴까요?' 라고 묻고 싶지만, 함수에 이름을 명시해놓는 편이 재사용 및 유지보수에 편하기 때문입니다.
  - 익명함수는 콜백함수로 많이 쓰입니다.
  ```Javascript
     setInterval(function(){
        console.log('wake up!')
    }, 1000);

  ```


## Inside_function
  - 함수 내부에서 함수를 호출할 수 있습니다.
  - 여러 개의 함수에서 쓰이는 기능(중복 코드)이나, 한 함수의 코드가 길어질 경우에는 외부 함수로 선언하여 진행하는 것이 좋습니다.
  - 이는 객체지향언어의 장점을 굳이 설명하지 않더라도 직관적으로 이해가 가능한 부분이나,
    차후 객체지향에 대한 개념을 배우고 난 후에는 더욱 잘 이해하실 수 있습니다.
  ```Javascript
    function function1() {
      function2();
    }
    function function2() {
      console.log("function2 called");  
    }
    function1();
  ```
  