// 입출력
// Hello World!를 출력하시오.
// g++ 2557.cc -o wow
// ./a.out

#include <iostream>

int main(){
    std::cout << "Hello World!" << std::endl;
    return 0;
}